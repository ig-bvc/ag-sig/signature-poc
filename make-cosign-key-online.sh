#! /bin/sh

name=$1

tempdir=$(mktemp -d)

here=$(pwd)
cd $tempdir
cosign generate-key-pair
mv cosign.key $here/$name.key
mv cosign.pub $here/$name.pub
cd $here
rm -rf $tempdir
exit 0